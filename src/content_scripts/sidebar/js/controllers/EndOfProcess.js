serviceCreator.controller('EndOfProcess', function($scope, $state, TemplateService) {
  AbstractController.call(this, $scope, $state, TemplateService);

  $scope.loadSubformBehaviour = function() {
    TemplateService.updateServices();
  };

  $scope.finishServiceDefinition = function() {
  	
  	TemplateService.saveService().then(function(service){
      browser.runtime.sendMessage({call: "closeSidebar"});
  	});
  };

  $scope.initialize();
});
