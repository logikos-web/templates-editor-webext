serviceCreator.controller('ServiceNameController', function($scope, $state, TemplateService) {
  AbstractController.call(this, $scope, $state, TemplateService);

  $scope.service = {
    name: "",
    url: undefined,
    itemType: ""
  };

  $scope.initialName;

  $scope.loadDataModel = function() {
    TemplateService.getService().then(function(service) {
      $scope.enableTagsAutocompletion().then(function(){

        if (service) {
          $scope.service.name = service.name;
          $scope.initialName = service.name;
          $scope.service.itemType = service.itemType;
          document.querySelector("#search_service_name").value = service.name;
          document.querySelector("#template_tag").value = service.itemType;
        }   

        $scope.focusElement("#search_service_name"); 
      });
    });

    browser.runtime.sendMessage({call: "getCurrentUrl"}).then(url => {
      var link = document.createElement("a");
      link.href = url;
      $scope.service.url = link.origin + "/*";
    });

    browser.runtime.sendMessage({call: "getCurrentFavicon"}).then(favIcon => {
      $scope.service.favIcon = favIcon;
    });
  };

  $scope.saveFullDataWithCurrentKey = function() {

    TemplateService.setCurrentServiceKey($scope.service.name);
    TemplateService.setName($scope.service.name).then(function() {
      TemplateService.updateServices();
      browser.runtime.sendMessage({call: "populateApisMenu"});
    });

    console.log("SAVING DATA ", $scope.service.itemType);

    TemplateService.setItemType($scope.service.itemType);
    TemplateService.setUrl($scope.service.url);
    TemplateService.setFavIcon($scope.service.favIcon);
  };

  $scope.saveDataModel = function() {

    if ($scope.service.name == undefined || $scope.service.name.trim() == '')
      return;

    if ($scope.initialName != undefined && $scope.initialName != $scope.service.name) {
      //update name
      TemplateService.updateServiceKey($scope.initialName, $scope.service.name).then(function() {
        $scope.saveFullDataWithCurrentKey();
      });
    } else
      $scope.saveFullDataWithCurrentKey();
    }
  ;
  $scope.saveUrl = function() {
    TemplateService.setUrl($scope.service.url);
  };

  $scope.getValidationRules = function() {
    return {
      "rules": {
        "search_service_name": {
          "minlength": 2,
          "required": true
        }
      },
      "messages": {
        search_service_name: browser.i18n.getMessage("this_field_is_required")
      }
    };
  }

  $scope.adaptPlaceholderExample = function(data) {
    document.querySelector("#search_service_name").setAttribute("placeholder", document.querySelector("#search_service_name").getAttribute("placeholder") + " " + data.domainName);
  };

  $scope.removeErrorMessages = function() {
    $scope.hideErrorMessage("nameAlreadyExistsError");
  };

  $scope.loadNextStep = function(nextState) {

    if ($scope.areRequirementsMet()) {
      TemplateService.uniqueNameService($scope.service.name).then(function(nameAlreadyExists) {

        if (!nameAlreadyExists) {

          $scope.saveDataModel();
          $scope.undoActionsOnDom();
          TemplateService.setBuildingStrategy("ExistingServiceEdition"); //Since the service is created, and the user may go forwards and back to this form ans he needs the new strategy to check for the uniqueName
          $state.go(nextState);
        } else {
          $scope.showErrorMessage("nameAlreadyExistsError", "#search_service_name", "service_name_already_exists");
          $scope.focusElement("#search_service_name");
        };
      });
    };
  };

  $scope.enableTagsAutocompletion = function(){

    return new Promise((resolve, reject) => {

      var ctrl = $('#template_tag');
      this.enableTags({ 
          ctrl: ctrl, 
          callback: function() {
              browser.runtime.sendMessage({call: 'getTemplateTags', args: {value: ctrl[0].value}}).then(function(tags){
                $scope.loadTemplateTags(tags);
              });
          }
      });
      resolve();
    });
  };

  $scope.enableTags = function(data){
    var me = this;
    data.ctrl.autocomplete({
        source: [],
        minLength: 1,
        select: function(event, ui) {
            event.preventDefault();
            $(this).val(ui.item.value); //.label
            $scope.service.itemType = ui.item.value;
        },
        change: function(event, ui) {
          $scope.service.itemType = (ui.item)? ui.item.value : event.target.value;
            /* This used to prevent that the user writes anything instead of picking an entity from the suggested tags 
            var me = $(this);
            if (ui.item === null) { 
                $scope.service.itemType = "";
                this.value = "";
            } */
            me.autocomplete('close');
        }
    });

    var searchTimeout;
    data.ctrl[0].onkeypress = function(evt){
        if(this==document.activeElement){
            if (evt.charCode != 0){
                if (searchTimeout != undefined) clearTimeout(searchTimeout);
                searchTimeout = setTimeout(data.callback, 250);  
            }
        }
    };
  }

  $scope.loadTemplateTags = function(tags){

    if( document.querySelector("#template_tag").style.display != 'none' ){
        var iTags = $(document.querySelector('#template_tag'));
            iTags.autocomplete("option", "source", tags);
            iTags.autocomplete( "search", iTags[0].value);
    } else console.log('Not loading tags');
  }

  $scope.initialize();
});