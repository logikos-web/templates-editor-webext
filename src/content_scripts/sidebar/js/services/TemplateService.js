//TODO: implement a state for being sure getMatchingServices is retrieving something when everything is fully loaded

function BuildingStrategy() {
  this.uniqueNameService = function(name, client, deferred) {};
}

function NewServiceEdition() {
  BuildingStrategy.call(this);

  this.uniqueNameService = function(name, client, deferred) {
    deferred.resolve(client.hasServiceNamed(name));
  }
  this.saveService = function(srv){

    return new Promise((resolve, reject) => {
      console.log("\n\nPOST");
      browser.runtime.sendMessage({"call": "sendToRemoteRepo", args: {"service": srv}});
      resolve();
    });
  }
}

function ExistingServiceEdition() {
  BuildingStrategy.call(this);

  this.uniqueNameService = function(name, client, deferred) {
    if (client.services[client.currentServiceKey].name == name) {
      deferred.resolve(false);
    } else {
      deferred.resolve(client.hasServiceNamed(name));
    }
  }
  this.saveService = function(srv){

    return new Promise((resolve, reject) => {
      console.log("\n\nUPDATE");
      browser.runtime.sendMessage({"call": "sendToRemoteRepo", args: {"service": srv}});
      resolve();
    });
  }
}

serviceCreator.service("TemplateService", [
  "$q",
  "$timeout",
  function($q, $timeout) {
    var $service = this;

    this.services;
    this.currentServiceKey;
    this.buildingStrategy;
    this.storage = new LocalStorage();

    this.initialize = function() {
      this.storage.get("services").then((storedServices) => {
        if (storedServices.services && Object.keys(storedServices.services).length > 0) {
          $service.services = storedServices.services;
        } else {
          $service.services = {}; //check if this is necessary. I thunk this is the def value, but just in case
        }
      });
    };

    this.hasServiceNamed = function(name) {
      var serviceExists = false;
      Object.keys($service.services).some(function(key, index) {
        if ($service.services[key].name == name) {
          serviceExists = true;
          return;
        }
      });
      return serviceExists;
    };

    this.getUrlDomain = function(url) {
      if (url) {
        var a = document.createElement('a');
        a.setAttribute('href', url);
        return a.hostname;
      }

      return "*";
    }
    this.getMatchingServices = function(url) {

      return new Promise((resolve, reject) => {

        browser.runtime.sendMessage({
           call: "getTemplatesFromRemoteRepo",
           args: {"key": url}
        }).then(function(services){

          $service.services = services;
          resolve($service.services);
        });
      });
    };

    this.newTemplateWithName = function(name) {
      return {
        name: name,
        urlPattern: "*",
        favIcon: "",
        groups: ["public"],
        id: undefined,
        owner: "no_reply@lifia.info.unlp.edu.ar",
        propertySelectors: {}
      };
    };

    this.asDeferred = function(action) {
      var deferred = $q.defer();
      $timeout(function() {
        if (action == undefined) {
          deferred.resolve();
        } else {
          const returnElem = action();
          deferred.resolve(returnElem);
        }
      }, 500);
      return deferred.promise;
    };

    this.logService = function() {
      this.asDeferred(function() {
        console.log($service.services[$service.currentServiceKey]);
        return;
      });
    };

    this.getService = function() { //Should be getCurrentService
      return this.asDeferred(function() {
        return $service.services[$service.currentServiceKey];
      });
    };

    this.saveService = function(){

      return new Promise((resolve, reject) => {
        $service.getService().then(function(srv){
          $service.getBuildingStrategy().then(function(buildStrat){
            buildStrat.saveService(srv);
            resolve();
          });
        });
      });
    };

    this.removeService = function(key) {

      return new Promise((resolve, reject) => {
        browser.runtime.sendMessage({"call": "deleteFromRemoteRepo", args: {"id": key}});
        $service.updateServices();
        resolve();
      });
    };

    this.uniqueNameService = function(name) {
      const deferred = $q.defer();
      this.buildingStrategy.uniqueNameService(name, $service, deferred);

      return deferred.promise;
    };

    this.setName = function(name) {
      const self = this;

      return this.asDeferred(function() {
        if ($service.services[$service.currentServiceKey] == undefined) {
          $service.services[$service.currentServiceKey] = self.newTemplateWithName(name);
        }

        $service.services[$service.currentServiceKey].name = name;
        return;
      });
    };

    this.updateServices = function() {
      this.storage.set("services", $service.services);
    };

    this.setInput = function(input) {
      return this.asDeferred(function() {
        $service.services[$service.currentServiceKey].input = input;
        $service.updateServices();
        return;
      });
    };

    this.setUrl = function(url) {
      return this.asDeferred(function() {
        $service.services[$service.currentServiceKey].urlPattern = url;
        return;
      });
    };

    this.setFavIcon = function(favIcon) {
      return this.asDeferred(function() {
        $service.services[$service.currentServiceKey].favIcon = favIcon;
        return;
      });
    };

    this.setItemType = function(itemType) {
      return this.asDeferred(function() {
        $service.services[$service.currentServiceKey].itemType = itemType;
        return;
      });
    };

    this.setTrigger = function(trigger) {
      return this.asDeferred(function() {
        $service.services[$service.currentServiceKey].trigger = trigger;
        $service.updateServices();
        return;
      });
    };

    this.setCurrentServiceKey = function(key) {
      return this.asDeferred(function() {
        $service.currentServiceKey = key;
        return;
      });
    };

    this.setResultsName = function(name) {
      return this.asDeferred(function() {
        $service.services[$service.currentServiceKey].results.name = name;
        return;
      });
    };

    this.setResultsPreview = function(preview) {
      return this.asDeferred(function() {
        $service.services[$service.currentServiceKey].results.preview = preview;
        return;
      });
    };

    this.updateServiceKey = function(oldKey, newKey) {
      return this.asDeferred(function() {
        $service.services[newKey] = $service.services[oldKey];
        delete $service.services[oldKey];
        $service.currentServiceKey = newKey;
        return;
      });
    };

    this.setBuildingStrategy = function(strategy) { // ExistingServiceEdition || NewServiceEdition
      return this.asDeferred(function() {
        $service.buildingStrategy = new window[strategy]();
        return;
      });
    };

    this.getBuildingStrategy = function(strategy) { // TODO: remove
      return this.asDeferred(function() {
        return $service.buildingStrategy;
      });
    };

    this.setProperties = function(properties) { // ExistingServiceEdition || NewServiceEdition
      //todo: add one by one, do not save an external collection
      return this.asDeferred(function() {
        $service.services[$service.currentServiceKey].propertySelectors = properties;
        return;
      });
    };

    this.initialize();
  }
]);
